import Test.Hspec
import Compiler
import BFPlayground
import BFYakParser
import Control.Monad (replicateM_, forM_)

toBF :: BrainFuck a -> String
toBF b = (\(_,x,_) -> x) $ runBF b emptyWorld

toReturnVal :: BrainFuck a -> a
toReturnVal b = (\(_,_,x) -> x) $ runBF b emptyWorld

exec b = last $ getWorlds (toBF b) ""

checkCells b = cells (exec b)

checkMem b = memory $ (\(x,_,_) -> x) $ runBF b emptyWorld

negateCellTest = hspec $ do
  describe "negateCell" $ do
    it "turns zero into 1" $ do
      (checkCells (right >> right >> negateCell 0 1)) `shouldBe` [1,0,0]
    it "turns one into 0" $ do
      (checkCells (inc >> right >> right >> negateCell 0 1)) `shouldBe` [0,0,0]

cellAndTest = hspec $ do
  describe "cellAnd" $ do
    it "t & t = t" $ do
      let truetrueprog = right >> inc >> right >> inc >> cellAnd 1 2 3 4 5 0
      (checkCells truetrueprog) `shouldBe` [1,0,0,0,0,0]
    it "t & f = f" $ do
      let truefalseprog = right >> inc >> cellAnd 1 2 3 4 5 0
      (checkCells truefalseprog) `shouldBe` [0,0,0,0,0,0]
    it "f & t = f" $ do
      let falsetrueprog = right >> right >> inc >> cellAnd 1 2 3 4 5 0
      (checkCells falsetrueprog) `shouldBe` [0,0,0,0,0,0]
    it "f & f = f" $ do
      let falsefalseprog = cellAnd 1 2 3 4 5 0
      (checkCells falsefalseprog) `shouldBe` [0,0,0,0,0,0]

cellOrTest = hspec $ do
  describe "cellOr" $ do
    it "t | t = t" $ do
      let truetrueprog = right >> inc >> right >> inc >> cellOr 1 2 3 0
      (checkCells truetrueprog) `shouldBe` [1,0,0,0]
    it "t | f = t" $ do
      let truefalseprog = right >> inc >> cellOr 1 2 3 0
      (checkCells truefalseprog) `shouldBe` [1,0,0,0]
    it "f | t = t" $ do
      let falsetrueprog = right >> right >> inc >> cellOr 1 2 3 0
      (checkCells falsetrueprog) `shouldBe` [1,0,0,0]
    it "f | f = f" $ do
      let falsefalseprog = cellOr 1 2 3 0
      (checkCells falsefalseprog) `shouldBe` [0,0,0,0]

cellXorTest = hspec $ do
  describe "cellXor" $ do
    it "does the damn xor" $ do
      let ttprog = right >> inc >> right >> inc >> cellXor 1 2 3 0
      checkCells ttprog `shouldBe` [0,0,0,0]
      let tfprog = right >> inc >> right >> cellXor 1 2 3 0
      checkCells tfprog `shouldBe` [1,0,0,0]
      let ftprog = right >> right >> inc >> cellXor 1 2 3 0
      checkCells ftprog `shouldBe` [1,0,0,0]
      let ffprog = right >> right >> cellOr 1 2 3 0
      checkCells ffprog `shouldBe` [0,0,0,0]

ifCellNonzeroTest = hspec $ do
  describe "ifCellNonzero" $ do
    it "branches properly" $ do
      let tprog = inc >> inc >> inc >> right >> right >> ifCellNonZero 0 (left >> inc >> right)
      checkCells tprog `shouldBe` [0,1,0]

copyCellTest = hspec $ do
  describe "copyCellTo" $ do
    it "copies the damn cell" $ do
      let cpprog = inc >> inc >> inc >> copyCellTo 0 3 4
      (checkCells cpprog) `shouldBe` [3,0,0,3,0]

moveCellTest = hspec $ do
  describe "moveCellTo" $ do
    it "moves the damn cell" $ do
      let mvprog = primitive "+++++" >> moveCellTo 0 3
      (checkCells mvprog) `shouldBe` [0,0,0,5]

moveRegionTest = hspec $ do
  describe "moveRegion" $ do
    it "moves the damn region" $ do
      let mvrprog = primitive "++>+++>+>+<<<" >> moveRegion (0,3) (6,9)
      checkCells mvrprog `shouldBe` [0,0,0,0,0,0,2,3,1,1]
      let mvrprog1 = primitive "++" >> moveRegion (0,0) (2,2)
      checkCells mvrprog1 `shouldBe` [0,0,2]

copyRegionTest = hspec $ do
  describe "copyRegion" $ do
    it "copies the damn region" $ do
      let cprprog = primitive "++>+++>+>+<<<" >> copyRegion (0,3) (6,9) 10
      (checkCells cprprog) `shouldBe` [2,3,1,1,0,0,2,3,1,1,0]

--this is the perfect candidate for property based testing
expressionTypeTest = hspec $ do
  describe "expressionType" $ do
    it "gets the type of Lookups" $ do
      let p = statement (Declare BoolType (Ref "bvar")) >> expressionType (Lookup (Ref "bvar"))
      toReturnVal p `shouldBe` BoolType
    it "gets the type of array Lookups" $ do
      let p = do
            statement (Declare (ArrayType (ArrayType IntType 2) 3) (Ref "jarray"))
            expressionType (Lookup (ArrayRef (Ref "jarray") (IntLiteral 1)))
      toReturnVal p `shouldBe` (ArrayType IntType 2)
      let p1 = do
            statement (Declare (ArrayType (ArrayType IntType 2) 3) (Ref "jarray"))
            expressionType (Lookup (ArrayRef (ArrayRef (Ref "jarray") (IntLiteral 1))
                                           (IntLiteral 1)))
      toReturnVal p1 `shouldBe` IntType
    it "gets the type of array expressions" $ do
      let exp = A [IntLiteral 1, IntLiteral 2, IntLiteral 2]
      toReturnVal (expressionType exp) `shouldBe` (ArrayType IntType 3)

typeSizeTest = hspec $ do
  it "gets the size of array types" $ do
    let t = typeSize (ArrayType (ArrayType BoolType 2) 3)
    t `shouldBe` 12

evalExpressionTest = hspec $ do
  let exprCells e = checkCells (evalExpression e)
  it "comares chars and bools for equality" $ do
    let help1 c1 c2 = exprCells (BoolCmp Equal (CharLit c1) (CharLit c2))
    help1 'a' 'b' `shouldBe` [0,0,0,0]
    help1 'a' 'a' `shouldBe` [1,0,0,0]
  it "converts int types" $ do
    exprCells (ToInt (UIntLiteral 22)) `shouldBe` [1,22]
    exprCells (ToUInt (IntLiteral 12)) `shouldBe` [12,0]

  it "evaluates literals properly" $ do
    exprCells (CharLit 'A') `shouldBe` [65]
    exprCells (IntLiteral 4) `shouldBe` [1,4]
    exprCells (IntLiteral 0) `shouldBe` [0,0]
    exprCells (IntLiteral (-5)) `shouldBe` [0,5]
    exprCells (UIntLiteral 34) `shouldBe` [34]
    exprCells (BoolLiteral True) `shouldBe` [1]
    exprCells (BoolLiteral False) `shouldBe` [0]
    exprCells (A [IntLiteral 0,
                  IntLiteral 3,
                  IntLiteral (-2)]) `shouldBe` [0,0,1,3,0,2,0]
  it "getsChars properly" $ do
    let p = toBF $ evalExpression (GetStr 4)
        w = last $ getWorlds p "string"
    cells w `shouldBe` [115,116,114,105,0]
  it "does comparison primitives" $ do
    let help1 x = exprCells (CmpPrim EQZero (IntLiteral x))
        help2 x = exprCells (CmpPrim GTZero (IntLiteral x))
    help1 0 `shouldBe` [1,0,0]
    help1 4 `shouldBe` [0,0,0]
    help1 (-3) `shouldBe` [0,0,0]
    help2 0 `shouldBe` [0,0]
    help2 3 `shouldBe` [1,0]
    help2 (-2) `shouldBe` [0,0]
  it "negates bools" $ do
    exprCells (BUnary Not (BoolLiteral True)) `shouldBe` [0,0]
    exprCells (BUnary Not (BoolLiteral False)) `shouldBe` [1,0]
  it "performs OR" $ do
    let help1 b1 b2 = exprCells (BBin BoolOr (BoolLiteral b1) (BoolLiteral b2))
    help1 True True `shouldBe` [1,0,0]
    help1 True False `shouldBe` [1,0,0]
    help1 False True `shouldBe` [1,0,0]
    help1 False False `shouldBe` [0,0,0]
  it "performs AND" $ do
    let help1 b1 b2 = exprCells (BBin BoolAnd (BoolLiteral b1) (BoolLiteral b2))
    help1 True True `shouldBe` [1,0,0,0,0]
    help1 True False `shouldBe` [0,0,0,0,0]
    help1 False True `shouldBe` [0,0,0,0,0]
    help1 False False `shouldBe` [0,0,0,0,0]
  it "negates integers" $ do
    exprCells (IUnary Negate (IntLiteral 3)) `shouldBe` [0,3,0,0]
    exprCells (IUnary Negate (IntLiteral (-4))) `shouldBe` [1,4,0,0]
  it "adds integers" $ do
    let help1 x y = exprCells (IBin IntPlus (IntLiteral x) (IntLiteral y))
    help1 3 2 `shouldBe` [1,5,0,0,0,0,0,0,0]
    help1 (-2) (-3) `shouldBe` [0,5,0,0,0,0,0,0,0]
    help1 3 (-6) `shouldBe` [0,3,0,0,0,0,0,0,0]
    help1 6 (-3) `shouldBe` [1,3,0,0,0,0,0,0,0]
    help1 (-6) 3 `shouldBe` [0,3,0,0,0,0,0,0,0]
    help1 (-3) 6 `shouldBe` [1,3,0,0,0,0,0,0,0]
    help1 (-1) 1 `shouldBe` [0,0,0,0,0,0,0,0,0]
    help1 1 (-1) `shouldBe` [0,0,0,0,0,0,0,0,0]
  it "adds uints" $ do
    exprCells (IBin IntPlus (UIntLiteral 3) (UIntLiteral 6)) `shouldBe` [9,0]
  it "subtracts integers" $ do
    exprCells (IBin IntMinus (IntLiteral 6) (IntLiteral 3)) `shouldBe` [1,3,0,0,0,0,0,0,0]
  it "subtracts uints" $ do
    exprCells (IBin IntMinus (UIntLiteral 65) (UIntLiteral 3)) `shouldBe` [62,0]
  it "multiplies integers" $ do
    let help1 x y = exprCells (IBin IntTimes (IntLiteral x) (IntLiteral y))
    help1 0 0 `shouldBe` [0,0,0,0,0,0,0]
    help1 2 0 `shouldBe` [0,0,0,0,0,0,0]
    help1 2 3 `shouldBe` [1,6,0,0,0,0,0]
    help1 (-2) 3 `shouldBe` [0,6,0,0,0,0,0]
    help1 (-3) 2 `shouldBe` [0,6,0,0,0,0,0]
    help1 (-3) (-2) `shouldBe` [1,6,0,0,0,0,0]
  it "multiplies uints" $ do
    exprCells (IBin IntTimes (UIntLiteral 4) (UIntLiteral 3)) `shouldBe` [12,0,0,0]
  it "divs integers" $ do
    exprCells (IBin IntDiv (IntLiteral 6) (IntLiteral 2)) `shouldBe` [1,3,0,0,0,0,0,0]
    exprCells (IBin IntDiv (IntLiteral (-6)) (IntLiteral 3)) `shouldBe` [0,2,0,0,0,0,0,0]
    exprCells (IBin IntDiv (IntLiteral 8) (IntLiteral 3)) `shouldBe` [1,2,0,0,0,0,0,0]
  it "divs uints" $ do
    exprCells (IBin IntDiv (UIntLiteral 9) (UIntLiteral 2)) `shouldBe` [4,0,0,0,0,0,0,0]
  it "does simple lookups" $ do
    let p = (Compiler.prog [Declare IntType (Ref "x"),
                           Assign (Ref "x") (IntLiteral 3)]) >>
            (movePointerTo =<< nextFreeMem) >>
            (evalExpression (Lookup (Ref "x")))
    cells (exec p) `shouldBe` [1,3,1,3,0]
  it "does array lookups" $ do
    let p = (statement (Declare (ArrayType IntType 4) (Ref "arraye"))) >>
            (statement (Assign (Ref "arraye") (A [IntLiteral 0,
                                                  IntLiteral (-1),
                                                  IntLiteral 3,
                                                  IntLiteral 1]))) >>
            (movePointerTo =<< nextFreeMem) >>
            (evalExpression (Lookup (ArrayRef (Ref "arraye") (IntLiteral 2))))
        p1 = (statement (Declare (ArrayType (ArrayType BoolType 2) 3) (Ref "arraye"))) >>
             (statement (Assign (Ref "arraye")
                                (A [A [BoolLiteral True, BoolLiteral True],
                                    A [BoolLiteral True, BoolLiteral False],
                                    A [BoolLiteral True, BoolLiteral True]]))) >>
             (movePointerTo =<< nextFreeMem)
        p2 = p1 >> (evalExpression (Lookup (ArrayRef (Ref "arraye") (IntLiteral 1))))
        p3 = p1 >> (evalExpression (Lookup (ArrayRef (ArrayRef (Ref "arraye")
                                                               (IntLiteral 1))
                                                     (IntLiteral 1))))
    cells (exec p) `shouldBe` [0,0,0,1,1,3,1,1,1,3,0,0,0,0,0,0,0,0,0]
    cells (exec p2) `shouldBe` [1,1,1,0,1,1,1,0,0,0,0,0,0,0,0]
    cells (exec p3) `shouldBe` [1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0]
  it "updates array Indices" $ do
    let p = Compiler.prog [Declare (ArrayType (ArrayType IntType 3) 3) (Ref "arr"),
                           Assign (Ref "arr") (A [A [IntLiteral 2, IntLiteral 2, IntLiteral 2],
                                               A [IntLiteral 2, IntLiteral 2, IntLiteral 2],
                                               A [IntLiteral 2, IntLiteral 2, IntLiteral 2]])]
        p' = p >> (movePointerTo =<< nextFreeMem)
        p1 = p' >> evalExpression (ArrayUpdate (Ref "arr") (IntLiteral 1) (A [IntLiteral 3,
                                                                             IntLiteral 3,
                                                                             IntLiteral 3]))
        p3 = Compiler.prog [Declare (ArrayType IntType 3) (Ref "arr"),
                            Assign (Ref "arr") (A [IntLiteral 3, IntLiteral 4, IntLiteral 5])]
             >> (movePointerTo =<< nextFreeMem)
             >> evalExpression (ArrayUpdate (Ref "arr") (IntLiteral 1) (IntLiteral (-3)))
    cells (exec p3) `shouldBe` [1,3,1,4,1,5,1,3,0,3,1,5,0,0,0,0,0,0,0,0,0]
    cells (exec p1) `shouldBe` [1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,
                                1,2,1,2,1,2,1,3,1,3,1,3,1,2,1,2,1,2,
                                0,0,0,0,0,0,0,0,0,0,0,0,0]

statementTest = hspec $ do
  it "declares properly" $ do
    let p = statement (Declare BoolType (Ref "abool"))
        p1 = statement (Declare (ArrayType IntType 2) (Ref "arraye"))
    checkMem p `shouldBe`  [VarEntry "abool" BoolType 0 0]
    checkMem p1 `shouldBe` [VarEntry "arraye" (ArrayType IntType 2) 0 3]
  it "assigns properly" $ do
    let p = Compiler.prog [Declare BoolType (Ref "abool"),
                           Assign (Ref "abool") (BoolLiteral True)]
        p1 = Compiler.prog [Declare (ArrayType IntType 2) (Ref "arraye"),
                            Assign (Ref "arraye") (A [IntLiteral 2, IntLiteral (-1)])]
    checkCells p `shouldBe` [1,0,0]
    checkCells p1 `shouldBe` [1,2,0,1,0,0,0,0,0]
  it "ifs properly" $ do
    let p b= Compiler.prog [Declare BoolType (Ref "x"),
                            IfStmt (BoolLiteral b) [Assign (Ref "x") (BoolLiteral True)]
                                                   [Assign (Ref "x") (BoolLiteral False)]]
    checkCells (p True) `shouldBe` [1,0,0]
    checkCells (p False) `shouldBe` [0,0,0]
  it "whiles properly" $ do
    let p = Compiler.prog [Declare IntType (Ref "x"),
                  Assign (Ref "x") (IntLiteral 3),
                  WhileStmt (CmpPrim GTZero (Lookup (Ref "x")))
                            [Assign (Ref "x")
                                    (IBin IntMinus (Lookup (Ref "x")) (IntLiteral 1))]]
    cells (exec p) `shouldBe` [0,0,0,0,0,0,0,0,0,0,0]
  it "puts strs properly" $ do
    let ptest xs = exec $ statement (PutStr (A (map CharLit xs)))
    -- let p = statement (PutStrLn (A [CharLit 'h', CharLit 'i', CharLit 'o']))
    cells (ptest "hio") `shouldBe` [0,0,0,0]
    outputChars (ptest "hio") `shouldBe` "hio"
    outputChars (ptest "\n") `shouldBe` "\n"
    -- let p1 = statement (PutStrLn)


main :: IO ()
main = do
  negateCellTest
  cellAndTest
  cellOrTest
  cellXorTest
  ifCellNonzeroTest
  copyCellTest
  moveCellTest
  moveRegionTest
  copyRegionTest
  expressionTypeTest
  evalExpressionTest
  statementTest
