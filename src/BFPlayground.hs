module BFPlayground where

import Control.Monad (replicateM_)


--BFProgram, stdin
--clear && printf '\e[3J'
-- printf '\033c'
-- printf '\033c\033[3J'


runPlayground :: String -> String -> IO ()
runPlayground p input = makeSpace >> go (getWorlds p input) 0 where
  go ws i = do
    render (ws !! i)
    c <- getChar
    case c of
      'h' -> if i == 0 then go ws i else go ws (i - 1)
      'l' -> if i >= length ws - 1 then go ws i else go ws (i + 1)
      'a' -> go ws (length ws - 1)
      'q' -> return ()

render :: WorldState -> IO ()
render w = do
  moveCursorBack
  replicateM_ 7 (eraseLine >> moveCursorUp)
  print w

data WorldState = WorldState {
    cells :: [Int]
  , pointer :: Int
  , inputChars :: String
  , outputChars :: String
  , prog :: Program
  , progPointer :: Int
} --deriving Show

makeSpace = putStr "\n\n\n\n\n\n\n\n"
moveCursorUp = putStr "\ESC[1A"
moveCursorBack = putStr "\ESC[1000D"
eraseLine = putStr "\ESC[K"

type Program = String

--Program: >++[->+<]
--         ^
--Cells  : [0][2][3][0][0][0][0]
--          ^
--input  : abcdef
--output : thereisnobusinesslikesnowhbusiness

instance Show WorldState where
  show (WorldState c p1 i o pr p2) =
    "Program: " ++ pr ++ "\n"
    ++ "         " ++ replicate p2 ' ' ++ "^" ++ "\n"
    ++ "Cells  : " ++ concatMap (\x -> "[" ++ show x ++ "]") c ++ "\n"
    ++ "         " ++ replicate ((+1) . sum . map (\n -> 2 + length (show n)) . take p1 $ c) ' ' ++ "^" ++ "\n"
    ++ "Input  : " ++ i ++ "\n"
    ++ "Output : " ++ o ++ "\n"



testProg1 = ">++[->+<]"
testProg2 = ">++[+[>]-]+[<]"
testWorld1 = WorldState [0] 0 "" "" testProg1 0
testWorld2 = WorldState [0] 0 "" "" testProg2 9

getWorlds :: Program -> String -> [WorldState]
getWorlds p input = go (WorldState [0] 0 input "" p 0) where
  go w = w : (case step w of
    Just x -> go x
    Nothing -> [])

editCellAtPointer :: WorldState -> (Int -> Int) -> WorldState
editCellAtPointer w f = let p = pointer w
                            cs = cells w
                        in w {cells = take p cs ++ [f (cs !! p)] ++ drop (p + 1) cs}


--prog pointer is at loop start
indexOfLoopEnd :: WorldState -> Int
indexOfLoopEnd w = go (progPointer w + 1) (prog w) 1 where
  go p cells count = case cells !! p of
    '[' -> go (p + 1) cells (count + 1)
    ']' -> if count == 1 then p else go (p + 1) cells (count - 1)
    _ -> go (p + 1) cells count

--prog pointer is point at loop end
indexOfLoopStart :: WorldState -> Int
indexOfLoopStart w = go (progPointer w - 1) (prog w) 1 where
  go p cells count = case cells !! p of
    ']' -> go (p - 1) cells (count + 1)
    '[' -> if count == 1 then p else go (p - 1) cells (count - 1)
    _ -> go (p - 1) cells count

step :: WorldState -> Maybe WorldState
step w
  | progPointer w >= length (prog w) = Nothing
step w = go (prog w !! progPointer w) w where
  go '+' w = Just $ (editCellAtPointer w (+1)) {progPointer = progPointer w + 1}
  go '-' w = Just $ (editCellAtPointer w (subtract 1)) {progPointer = progPointer w + 1}
  go '<' w
    | pointer w == 0 = Just $ w {cells = 0:cells w, progPointer = progPointer w + 1}
    | otherwise = Just $ w {pointer = pointer w - 1, progPointer = progPointer w + 1}
  go '>' w
    | pointer w == length (cells w) - 1 = Just $ w {cells = cells w ++ [0]
                                           , pointer = pointer w + 1
                                           , progPointer = progPointer w + 1}
    | otherwise = Just $ w {pointer = pointer w + 1, progPointer = progPointer w + 1}
  go ',' w = Just $ (editCellAtPointer w (const . fromEnum . head . inputChars $ w))
    {progPointer = progPointer w + 1, inputChars = tail (inputChars w)}
  go '.' w = Just $ w {outputChars = outputChars w ++ [toEnum (cells w !! pointer w)],
                       progPointer = progPointer w + 1}
  go '[' w
    | cells w !! pointer w == 0 = Just $ w {progPointer = indexOfLoopEnd w + 1}
    | otherwise = Just $ w {progPointer = progPointer w + 1}
  go ']' w = Just $ w {progPointer = indexOfLoopStart w}
