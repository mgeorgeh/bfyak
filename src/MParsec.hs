module MParsec where

import Control.Applicative hiding (many)

data Parser a = Parser (String -> [(a,String)])

item = Parser (\s -> case s of
                     ""     -> []
                     (c:cs) -> [(c,cs)])

parse (Parser p) = p

class (Monad m) => MonadPlus m where
  mzero :: m a
  mplus :: m a -> m a -> m a

instance Monad Parser where
  (Parser p) >>= f = Parser (\s -> (p s) >>= (\(a,s') -> parse (f a) s'))
  return a = Parser (\s -> [(a,s)])

instance MonadPlus Parser where
  mzero = Parser (\cs -> [])
  mplus p q = Parser (\s -> (parse p) s ++ (parse q) s)

instance Functor Parser where
  --fmap :: (a -> b) -> Parser a -> Parser b
  fmap f p = Parser (\s -> fmap (\(a,s') -> (f a,s')) $ parse p s)

instance Applicative Parser where
  -- (<*>) ::Parser (a -> b) -> Parser a -> Parser b
  pure = return
  f <*> p = Parser (\s -> let f' = fst.head.parse f $ s
                              p' = fst.head.parse p $ s
                          in [((f' p'),s)])

--try :: Parser a -> Parser a
--try p = Parser (\s -> case parse p s of
--                   [] -> [(,s)]
--                   l -> l)

option :: Parser a -> Parser a -> Parser a
option p q = Parser (\s -> case parse (mplus p q) s of
                           [] -> []
                           (x:xs) -> [x])

(<|>) :: Parser a -> Parser a -> Parser a
p <|> q = option p q

satisfies :: (Char -> Bool) -> Parser Char
--satisfies p = item >>= \c -> (if p c then return c else mzero)
satisfies p = do
  c <- item
  if p c then return c else mzero

char :: Char -> Parser Char
char c = satisfies (c ==)

string :: String -> Parser String
string "" = return ""
string (c:cs) = char c >> string cs >> return (c:cs)

many :: Parser a -> Parser [a]
many p = many1 p `option` return []

many1 :: Parser a -> Parser [a]
many1 p = do
  a <- p
  as <- many p
  return (a:as)

sepBy :: Parser a -> Parser b -> Parser [a]
p `sepBy` sep = (p `sepBy` sep) `option` return []

sepBy1 :: Parser a -> Parser b -> Parser [a]
p `sepBy1` sep = do
  a <- p
  as <- many (sep >> p)
  return (a:as)

chainl :: Parser a -> Parser (a -> a -> a) -> a -> Parser a
chainl p op a = (p `chainl1` op) `option` return a

chainl1 :: Parser a -> Parser (a -> a -> a) -> Parser a
p `chainl1` op = p >>= \a -> rest a where
--p `chainl1` op = do {a <- p; rest a} where
  rest a = (do {f <- op; b <- p; rest (f a b)}) `option` return a

oneOf :: String -> Parser Char
oneOf cs = satisfies (\c -> elem c cs)

space :: Parser String
space = many (oneOf " \t\n")

token :: Parser a -> Parser a
token p = do
  space
  a <- p
  space
  return a

symb :: String -> Parser String
symb cs = token (string cs)

--very odd name
apply :: Parser a -> String -> [(a,String)]
apply p = parse (space >> p)


alpha :: Parser Char
alpha = oneOf "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz"
