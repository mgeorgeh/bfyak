module Compiler where

import BFYakParser
import Data.Monoid ((<>))
import Control.Monad (replicateM_, unless, liftM2, forM_)
-- world in compiler and playground. one should have a different name

type Name = String
data World = World { pointerPos :: Int
                   , memory :: [VarEntry]
                   } deriving (Show, Eq)

data VarEntry = VarEntry { name :: String
                         , typ :: Type
                         , start :: Int
                         , end :: Int} deriving (Show, Eq)
type Program = String
newtype BrainFuck a = BrainFuck {runBF :: World -> (World, Program, a)}

instance Functor BrainFuck where
  fmap f (BrainFuck g) = BrainFuck (\w -> let (world, prog, a) = g w
                                          in  (world, prog, f a))

instance Applicative BrainFuck where
  pure x = BrainFuck (\w -> (w, mempty, x))
  (BrainFuck f) <*> (BrainFuck x) = BrainFuck (\w -> let (w1, p1, val) = x w
                                                         (w2, p2, func) = f w1
                                                     in (w2, p1 <> p2, func val))

instance Monad BrainFuck where
  return = pure
  (BrainFuck x) >>= f = BrainFuck (\w -> let (w1, p1, a) = x w
                                             (BrainFuck g) = f a
                                             (w2, p2, b) = g w1
                                         in (w2, p1 <> p2, b))

emptyWorld = World 0 []

world :: BrainFuck World
world = BrainFuck $ \w -> (w,"",w)

modifyWorld :: (World -> World) -> BrainFuck ()
modifyWorld f = BrainFuck $ \w -> (f w, "", ())

incPointer :: Int -> World -> World
incPointer n (World p a) = World (p + n) a

find :: String -> World -> VarEntry
find s w = go s (memory w) where
  go s [] = error $ "attempt to find nonexistent variable" ++ (show s)
  go s (x:xs)
    | s == name x = x
    | otherwise = go s xs

primitive :: String -> BrainFuck ()
primitive s = BrainFuck $ \w -> (w, s, ())

inc, dec, putC, getC, right, right', left, left', noop:: BrainFuck ()
inc = primitive "+"
dec = primitive "-"
putC = primitive "."
getC = primitive ","
right = modifyWorld (incPointer 1) >> primitive ">"
left = modifyWorld (incPointer (-1)) >> primitive "<"
--used when it is not determinable where the pointer should go. use sparingly
right' = primitive ">"
left' = primitive "<"
noop = primitive ""

--loops must not change the pointer position. cannot influence memory (more than once)
loop :: BrainFuck a -> BrainFuck ()
loop x = primitive "[" >> x >> primitive "]"

pointer :: BrainFuck Int
pointer = pointerPos <$> world

nextFreeMem :: BrainFuck Int
nextFreeMem = ((\x -> if null x then 0 else maximum x + 1) . map end . memory) <$> world

--var can only be a ref
--size of array must be known at compile time
addToMemory :: Var -> Type -> Int -> Int -> BrainFuck ()
addToMemory (Ref n) t s e = modifyWorld (\w -> w {memory = VarEntry n t s e:memory w})

--takes var, returns memory start and end.
--only call with things known at compile time
--INCLUSIVE RANGES
memLoc :: Var -> BrainFuck (Int,Int)
memLoc (Ref s) =  (liftM2 (,) start end . find s) <$> world
memLoc (ArrayRef _ _) = error "how did you get here?"

compile :: BFYakProg -> String
compile p = (\(_,x,_) -> x) $ runBF (prog p) emptyWorld

prog :: BFYakProg -> BrainFuck ()
prog = mapM_ statement

statement :: BFYakStatement -> BrainFuck ()
statement (Declare t v) = do
  m <- nextFreeMem
  addToMemory v t m (m + typeSize t - 1)

-- assumes name exists
statement (Assign (Ref name) e) = balanced $ do
  movePointerTo =<< nextFreeMem
  evalExpression e
  (a,b) <- memLoc (Ref name)
  p <- pointer
  t <- expressionType e
  mapM_ zeroCell [a..b]
  moveRegion (p, p + typeSize t - 1) (a,b)

statement (Assign v@(ArrayRef _ _) e) = statement (Assign (underlyingRef v)
                                                   (mkUpdateExpression v e)) where
  underlyingRef v@(Ref _) = v
  underlyingRef (ArrayRef v _) = underlyingRef v

  mkUpdateExpression (ArrayRef v@(Ref _) i) e = ArrayUpdate v i e
  mkUpdateExpression (ArrayRef v@(ArrayRef _ _) i) e = mkUpdateExpression v (ArrayUpdate v i e)

statement (IfStmt cond t e) = balanced $ do
  movePointerTo =<< nextFreeMem
  p <- pointer
  evalExpression cond
  ifCellNonZero p (prog t)
  evalExpression (BUnary Not cond)
  ifCellNonZero p (prog e)

statement (WhileStmt cond body) = balanced $ do
  let loopCond = do
        movePointerTo =<< nextFreeMem
        evalExpression cond
  p <- pointer
  loopCond
  loop $ do
    zeroCellAtPointer
    movePointerTo p
    prog body
    loopCond

--exp can only be array of things of size 1
statement (PutStr e) = balanced $ do
  movePointerTo =<< nextFreeMem
  evalExpression e
  s <- expressionSize e
  replicateM_ s $ putC >> zeroCellAtPointer >> right

-- assume pointer is at the first cell where it should write its output
-- ends up at the same place
evalExpression :: Expr -> BrainFuck ()
evalExpression (Lookup v@(Ref _)) = balanced $ do
  (a,b) <- memLoc v
  p <- pointer
  copyRegion (a,b) (p, p + (b - a)) (p + (b - a) + 1)

evalExpression exp@(Lookup (ArrayRef v e)) = balanced $ do
  evalExpression e
  right >> right
  let e' = Lookup v
  eltSize <- expressionSize exp
  totSize <- expressionSize e'
  evalExpression e'
  left
  p <- pointer
  loop $ do
    dec
    forM_ [1..eltSize] $ \n -> zeroCell (p + n)
    forM_ [eltSize..totSize] $ \n -> moveCellTo (p + n) (p + n - eltSize)
  zeroCell (p - 1)
  forM_ [1..eltSize] $ \n -> moveCellTo (p + n) (p + n - eltSize)
  forM_ [1..totSize] $ \n -> zeroCell (p + n)

evalExpression (ArrayUpdate v indx e) = balanced $ do
  p <- pointer
  evalExpression indx
  eltSize <- expressionSize e
  right >> right >> right
  evalExpression e
  replicateM_ eltSize right
  evalExpression (Lookup v)
  arrSize <- expressionSize (Lookup v)
  let arrLen = arrSize `div` eltSize
  replicateM_ (arrSize - 1) right
  forM_ [arrLen,arrLen-1..1] $ \n -> do
    replicateM_ eltSize $ do
      loop $ do
        dec
        replicateM_ n right
        inc
        replicateM_ n left
      left
  right >> inc
  replicateM_ (eltSize + 2) left
  let seekRight = loop $ replicateM_ (eltSize + 1) right'
      seekLeft = loop $ replicateM_ (eltSize + 1) left'
  loop $ do
    replicateM_ (eltSize + 2) right'
    seekRight
    inc
    seekLeft
    left'
    dec
  replicateM_ (eltSize + 2) right
  seekRight
  replicateM_ eltSize $ left' >> zeroCellAtPointer
  left'
  seekLeft
  replicateM_ (eltSize + 1) right'
  forM_ [eltSize, eltSize - 1..1] $ \n -> do
    replicateM_ n left
    loop $ do
      dec
      replicateM_ n right'
      seekRight
      replicateM_ n left'
      inc
      replicateM_ (eltSize + 1 - n) left'
      seekLeft
      replicateM_ (eltSize + 1 - n) right'
    replicateM_ n right
  p' <- pointer
  replicateM_ (eltSize + 3) $ left >> zeroCellAtPointer
  movePointerTo p'
  forM_ [0..arrLen - 1] $ \n -> do
    zeroCellAtPointer
    c <- pointer
    forM_ [1..eltSize] $ \m -> do
      moveCellTo (c + m) (p + n*eltSize + m - 1)
    replicateM_ (eltSize + 1) right

-- eval e1, right, eval e2, branch on op
evalExpression (BBin op e1 e2) = balanced $ do
  evalExpression e1
  right
  evalExpression e2
  left
  p <- pointer
  case op of
    BoolAnd -> cellAnd p (p+1) (p+2) (p+3) (p+4) p
    BoolOr -> cellOr p (p+1) (p+2) p

evalExpression (BUnary op e) = case op of
  Not -> balanced $ do
    evalExpression e
    p <- pointer
    negateCell p (p+1)

evalExpression (IBin IntMinus e1 e2) = balanced $ do
  t <- expressionType e1
  case t of
    --assuming that both e1 and e2 are uint
    --and that the operation can be performed
    UIntType -> do
      evalExpression e1
      right
      evalExpression e2
      loop $ dec >> left >> dec >> right
    _ -> balanced $ evalExpression (IBin IntPlus e1 (IUnary Negate e2))

evalExpression (IBin IntDiv e1 e2) = balanced $ do
  t <- expressionType e1
  case t of
    UIntType -> evalExpression (ToUInt (IBin IntDiv (ToInt e1) (ToInt e2)))
    _ -> do
      evalExpression e1
      replicateM_ (typeSize t) right
      p <- pointer
      let x = p - 1; sigx = p - 2; y = p + 1; sigy = p
      evalExpression e2
      let tmp0 = p + 2; tmp1 = p + 3; tmp2 = p + 4; tmp3 = p + 5
      moveCellTo x tmp0
      movePointerTo tmp0
      loop $ do
        movePointerTo y
        loop $ do
          movePointerTo tmp1
          inc
          movePointerTo tmp2
          inc
          movePointerTo y
          dec
        moveCellTo tmp2 y
        movePointerTo tmp1
        loop $ do
          movePointerTo tmp2
          inc
          movePointerTo tmp0
          dec
          loop $ do
            zeroCell tmp2
            movePointerTo tmp3
            inc
            movePointerTo tmp0
            dec
          moveCellTo tmp3 tmp0
          movePointerTo tmp2
          loop $ do
            movePointerTo tmp1
            dec
            loop $ do
              movePointerTo x
              dec
              movePointerTo tmp1
              zeroCellAtPointer
            inc
            movePointerTo tmp2
            dec
          movePointerTo tmp1
          dec
        movePointerTo x
        inc
        movePointerTo tmp0
      zeroCell y
      moveCellTo sigx tmp0
      moveCellTo sigy tmp1
      cellXor tmp0 tmp1 tmp2 sigx
      negateCell sigx tmp0

evalExpression (IBin op e1 e2) = balanced $ do
  t <- expressionType e1
  evalExpression e1
  replicateM_ (typeSize t) right
  evalExpression e2
  p <- pointer
  case t of
    IntType -> do
      let x = p - 1; sigx = p - 2; y = p + 1; sigy = p
      case op of
        IntPlus -> do
          copyCellTo sigx (p + 2) (p + 4)
          copyCellTo sigy (p + 3) (p + 4)
          cellXor (p + 2) (p + 3) (p + 4) (p + 5)
          copyCellTo (p + 5) (p + 6) (p + 4)
          negateCell (p + 6) (p + 4)

          ifCellNonZero (p + 6) $ do
            moveCellTo y x
          ifCellNonZero (p + 5) $ do
            let loopCond = ((copyCellTo x (p + 2) (p + 3)) >>
                           (copyCellTo y (p + 3) (p + 4)) >>
                           (cellAnd (p + 2) (p + 3) (p + 4) (p + 5) (p + 6) (p + 2)) >>
                           (movePointerTo (p + 2)))
            loopCond
            loop $ do
              zeroCellAtPointer
              movePointerTo x
              dec
              movePointerTo y
              dec
              loopCond
            copyCellTo y (p + 3) (p + 4)
            ifCellNonZero (p + 3) $ do
              zeroCell sigx
              zeroCell x
              moveCellTo sigy sigx
              moveCellTo y x
          zeroCell sigy
          copyCellTo x p (p + 1)
          negateCell p (p + 1)
          ifCellNonZero p (zeroCell sigx)

        IntTimes -> do
          right
          loop $ do
            copyCellTo x (p + 2) (p + 3)
            dec
          zeroCell x
          moveCellTo (p + 2) x
          moveCellTo sigx (p + 2)
          moveCellTo sigy (p + 3)
          cellXor (p + 2) (p + 3) (p + 4) sigx
          negateCell sigx (p + 4)
          copyCellTo x sigy (p + 4)
          negateCell sigy y
          ifCellNonZero sigy $ zeroCell sigx
    _ -> case op of -- implicitly assuming that type has size 1
      IntPlus -> moveCellTo p (p - 1)
      IntTimes -> do
        let x = p - 1; y = p; tmp0 = p+1; tmp1 = p+2
        loop $ dec >> copyCellTo x tmp0 tmp1
        zeroCell x
        moveCellTo tmp0 x

evalExpression (IUnary Negate e) = balanced $ do
  evalExpression e
  p <- pointer
  copyCellTo (p + 1) (p + 2) ( p + 3)
  ifCellNonZero (p + 2) (negateCell p (p+3))

--THIS SECTION NOT YET TESTED
evalExpression (BoolCmp Equal e1 e2) = balanced $ do
  t <- expressionType e1
  if t `elem` [CharType, BoolType, UIntType] then do
    p <- pointer
    evalExpression e1
    right
    evalExpression e2
    cellXor p (p+1) (p+2) (p+3)
    negateCell (p+3) (p+1)
    moveCellTo (p+3) p
  else evalExpression $ CmpPrim EQZero (IBin IntMinus e1 e2)

evalExpression (BoolCmp op e1 e2) = evalExpression $ case op of
  GTEqual -> BBin BoolOr (BoolCmp GreaterThan e1 e2) (BoolCmp Equal e1 e2)
  LTEqual -> BBin BoolOr (BoolCmp LessThan e1 e2) (BoolCmp Equal e1 e2)
  LessThan -> CmpPrim GTZero (IBin IntMinus e2 e1)
  GreaterThan -> CmpPrim GTZero (IBin IntMinus e1 e2)
  NotEqual -> BUnary Not (BoolCmp Equal e1 e2)

evalExpression (BoolLiteral b) = if b then inc else noop
evalExpression (IntLiteral i) = balanced $ do
  if i > 0 then inc else noop
  right
  replicateM_ (abs i) inc

evalExpression (UIntLiteral i) = replicateM_ i inc
evalExpression (ToUInt e) = balanced $ do
  evalExpression e
  p <- pointer
  zeroCellAtPointer
  moveCellTo (p+1) p

evalExpression (ToInt e) = balanced $ do
  inc
  right
  evalExpression e
  left

evalExpression (CharLit c) = replicateM_ (fromEnum c) inc
evalExpression (CmpPrim op e) = balanced $ do
  p <- pointer
  evalExpression e
  right
  case op of
    EQZero -> do
      negateCell (p + 1) (p + 2)
      zeroCell p
      moveCellTo (p + 1) p
    GTZero -> zeroCellAtPointer

evalExpression (GetStr i) = balanced $ replicateM_ i (getC >> right)
evalExpression (A arrexp) = balanced $ forM_ arrexp $ \e -> do
  evalExpression e
  s <- expressionSize e
  replicateM_ s right

balanced :: BrainFuck () -> BrainFuck ()
balanced b = do
  p <- pointer
  b
  movePointerTo p

typeSize :: Type -> Int
typeSize IntType = 2
typeSize CharType = 1
typeSize BoolType = 1
typeSize UIntType = 1
typeSize (ArrayType t i) = i * typeSize t

expressionType :: Expr -> BrainFuck Type
expressionType (Lookup (Ref s)) = (typ . find s) <$> world
expressionType (Lookup (ArrayRef v _)) = removeNesting <$> expressionType (Lookup v) where
  removeNesting (ArrayType t _) = t
expressionType (ArrayUpdate v _ _) = expressionType (Lookup (ArrayRef v (IntLiteral 0)))
expressionType (A arrexp) = case arrexp of
  [] -> return $ ArrayType BoolType 0
  (x:_) -> do
    t <- expressionType x
    return $ ArrayType t (length arrexp)
expressionType t = return $ go t where
  go BBin{} = BoolType
  go BUnary{} = BoolType
  go IBin{} = IntType
  go BoolCmp{} = BoolType
  go CmpPrim{} = BoolType
  go (BoolLiteral _) = BoolType
  go (IntLiteral _) = IntType
  go (UIntLiteral _) = UIntType
  go (CharLit _) = CharType
  go (ToInt _) = IntType
  go (ToUInt _) = UIntType
  go (GetStr i) = ArrayType CharType i

expressionSize :: Expr -> BrainFuck Int
expressionSize e = typeSize <$> expressionType e

--INCLUSIVE RANGES
--MUST BE SAME LENGTH
--DO NOT OVERLAP
--(START, END), start < end
moveRegion :: (Int,Int) -> (Int,Int) -> BrainFuck ()
moveRegion (a,b) (c,d) = balanced $ do
  movePointerTo a
  mapM_ (\n -> moveCellTo (a + n) (c + n)) [0..b - a]

copyRegion :: (Int,Int) -> (Int,Int) -> Int -> BrainFuck ()
copyRegion (a,b) (c,d) tmp = balanced $ do
  movePointerTo a
  mapM_ (\n -> copyCellTo (a + n) (c + n) tmp) [0..b - a]

moveCellTo :: Int -> Int -> BrainFuck ()
moveCellTo source dest = balanced $ do
  unless (source == dest) $ do
    movePointerTo source
    loop $ do
      dec
      movePointerTo dest
      inc
      movePointerTo source

copyCellTo :: Int -> Int -> Int -> BrainFuck ()
copyCellTo source dest tmp = balanced $ do
  movePointerTo source
  loop $ do
    dec
    movePointerTo dest
    inc
    movePointerTo tmp
    inc
    movePointerTo source
  moveCellTo tmp source

ifCellNonZero :: Int -> BrainFuck () -> BrainFuck ()
ifCellNonZero c x = balanced $ do
  p <- pointer
  movePointerTo c
  loop $ do
    zeroCellAtPointer
    movePointerTo p
    x
    movePointerTo c

cellXor :: Int -> Int -> Int -> Int -> BrainFuck ()
cellXor x y tmp result = balanced $ do
  let negateres = loop $ do
        dec
        negateCell result tmp
  movePointerTo x
  negateres
  movePointerTo y
  negateres

cellOr :: Int -> Int -> Int -> Int -> BrainFuck ()
cellOr x y tmp result = balanced $ do
  moveCellTo x tmp
  moveCellTo y tmp
  ifCellNonZero tmp $ do
    movePointerTo result
    inc

cellAnd :: Int -> Int -> Int -> Int -> Int -> Int -> BrainFuck ()
cellAnd x y nx x' tmp result = balanced $ do
  copyCellTo x nx tmp
  copyCellTo x x' tmp
  negateCell nx tmp
  ifCellNonZero x' $ do
    zeroCell x
    moveCellTo y result
  ifCellNonZero nx $ do
    zeroCell y
    zeroCell x

zeroCellAtPointer :: BrainFuck ()
zeroCellAtPointer = loop dec

zeroCell :: Int -> BrainFuck ()
zeroCell x = balanced $ movePointerTo x >> loop dec

negateCell :: Int -> Int -> BrainFuck ()
negateCell x tmp = balanced $ do
  moveCellTo x tmp
  movePointerTo x
  inc
  ifCellNonZero tmp $ do
    movePointerTo x
    dec

movePointerTo :: Int -> BrainFuck ()
movePointerTo loc = do
  p <- pointer
  replicateM_ (abs (p - loc)) (if p > loc then left else right)
