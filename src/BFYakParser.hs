module BFYakParser where

import MParsec

type BFYakProg = [BFYakStatement]

data BFYakStatement = Declare Type Var
                  | Assign Var Expr
                  | IfStmt Expr BFYakProg BFYakProg
                  | WhileStmt Expr BFYakProg
                  | PutStr Expr deriving (Show,Eq)

data Type = IntType | CharType | BoolType
          | UIntType
          | ArrayType Type Int deriving (Show,Eq)

data Var = Ref String | ArrayRef Var Expr deriving (Show,Eq)

data Expr = Lookup Var
          | ToInt Expr
          | ToUInt Expr
          | ArrayUpdate Var Expr Expr
          | BBin BBinOp Expr Expr
          | BUnary BUnOp Expr
          | IBin IBinOp Expr Expr
          | IUnary IUnOp Expr
          | BoolCmp CmpOp Expr Expr
          | BoolLiteral Bool
          | IntLiteral Int
          | UIntLiteral Int
          | CharLit Char
          | CmpPrim PrimOp Expr
          | GetStr Int
          | A ArrayExp deriving (Show,Eq)

data PrimOp = EQZero | GTZero deriving (Show,Eq)

data CmpOp = Equal | GTEqual | LTEqual | LessThan
           | GreaterThan | NotEqual deriving (Show,Eq)

data BBinOp = BoolAnd | BoolOr deriving (Show,Eq)

data BUnOp = Not deriving (Show,Eq)

data IBinOp = IntPlus | IntMinus | IntTimes | IntDiv deriving (Show,Eq)

data IUnOp = Negate deriving (Show,Eq)

type ArrayExp = [Expr]

getProg :: String -> BFYakProg
getProg = fst . head . parse parseProg

parseProg :: Parser BFYakProg
parseProg = many parseStatement

parseStatement :: Parser BFYakStatement
parseStatement = space >> (parseDeclare
                          <|> parseAssign
                          <|> parseIf
                          <|> parseWhile
                          <|> parsePutStr)

parseDeclare :: Parser BFYakStatement
parseDeclare = do
  t <- parseType
  char ' '
  n <- parseVar
  char ';'
  return (Declare t n)

parseType :: Parser Type
parseType = parseArrayType <|> parseNakedType

parseNakedType :: Parser Type
parseNakedType = mkNakedType <$> (string "int"
                                  <|> string "char"
                                  <|> string "bool"
                                  <|> string "uint")

mkNakedType s = case s of
  "int" -> IntType
  "char" -> CharType
  "bool" -> BoolType
  "uint" -> UIntType
  _ -> error $ "invalid input to fromStr: " ++ s

parseArrayType :: Parser Type
parseArrayType = do
  base <- parseNakedType
  nesting <- many1 parseBracketInt
  return $ foldl ArrayType base nesting

parseBracketInt :: Parser Int
parseBracketInt = do
  char '['
  i <- parseInt
  char ']'
  return i

digit = oneOf "1234567890"

parseInt :: Parser Int
parseInt = toInt <$> many1 digit

parseUInt = string "u" >> parseInt

toInt :: String -> Int
toInt = foldl (\x y -> singleChar y + 10*x) 0

singleChar :: Char -> Int
singleChar c = fromEnum c - fromEnum '0'

between :: Parser a -> Parser b -> Parser c -> Parser c
between a b c = do
  a
  e <- c
  b
  return e

parseIdentifier :: Parser String
parseIdentifier = many1 (alpha <|> digit)

parseVar :: Parser Var
--name[5][6][7] -> s = "name", nest = [5,6,7]
-- => ArrayRef (ArrayRef (ArrayRef (Ref name) (IntLiteral 5)) (IntLiteral 6)) (IntLiteral 7)
parseVar = (do {s <- parseIdentifier;
                nesting <- many1 (do {symb "[";
                                      e <- parseStdExpr;
                                      symb "]";
                                      return e;});
                return $ foldl ArrayRef (Ref s) nesting;})
           <|> (do {s <- parseIdentifier; return $ Ref s})

parseAssign :: Parser BFYakStatement
parseAssign = do
  name <- parseVar
  symb "="
  exp <- parseExpr
  symb ";"
  return $ Assign name exp

parseExpr :: Parser Expr
parseExpr = parseArrayExpr
            <|> parseCharExpr
            <|> parseStdExpr
            <|> parseLookup

parseStdExpr = parseTerm `chainl1` parseAddOp

parseTerm = parseFactor `chainl1` parseMulOp

parseFactor = parseLiteral
              <|> parseGetStr
              <|> parseNot
              <|> parseLookup
              <|> do {symb "("; e <- parseExpr; symb ")"; return e}

parseLiteral = parseBoolLit <|> (UIntLiteral <$> parseUInt) <|> (IntLiteral <$> parseInt)

parseAddOp = do
  c <- symb "+"
        <|> symb "-"
        <|> symb "||"
        <|> symb "=="
        <|> symb ">="
        <|> symb "<="
        <|> symb "<"
        <|> symb ">"
        <|> symb "!="
  let op = case c of
        "+" -> IBin IntPlus
        "-" -> IBin IntMinus
        "||" -> BBin BoolOr
        "==" -> BoolCmp Equal
        ">=" -> BoolCmp GTEqual
        "<=" -> BoolCmp LTEqual
        "<" -> BoolCmp LessThan
        ">" -> BoolCmp GreaterThan
        "!=" -> BoolCmp NotEqual
        _ -> error $ "Invalid add op: " ++ c
  return op

parseMulOp = do
  c <- symb "*" <|> symb "/" <|> symb "&&"
  let op = case c of
        "*" -> IBin IntTimes
        "/" -> IBin IntDiv
        "&&" -> BBin BoolAnd
        _ -> error $ "Invalid Mult op: " ++ c
  return op

parseCharExpr :: Parser Expr
parseCharExpr = between (char '\'') (char '\'') $ do
  c <- item
  c' <- case c of
    '\\' -> do
      c2 <- item
      return $ case c2 of
        'n' -> '\n'
        't' -> '\t'
    _ -> return c
  return $ CharLit c'

parseGetStr :: Parser Expr
parseGetStr = do
  symb "gets("
  i <- parseInt
  symb ")"
  return $ GetStr i

parseNot = do
  symb "!"
  f <- parseFactor
  return $ BUnary Not f

parseBoolLit = (do {symb "true"; return $ BoolLiteral True})
            <|> (do {symb "false"; return $ BoolLiteral False})

parseLookup = do
  space
  v <- parseVar
  space
  return $ Lookup v

parseArrayExpr :: Parser Expr
parseArrayExpr = parseEmptyArr <|> parseMultiArr

parseEmptyArr = do
  symb "{"
  symb "}"
  return $ A []

parseMultiArr :: Parser Expr
parseMultiArr = do
  symb "{"
  cs <- many (do {e <- parseExpr; symb ","; return e})
  c <- parseExpr
  symb "}"
  return $ A (cs ++ [c])

parseIf = do
  symb "if"
  e <- parseExpr
  symb "{"
  thenClause <- parseProg
  symb "}"
  symb "else"
  symb "{"
  elseClause <- parseProg
  symb "}"
  return $ IfStmt e thenClause elseClause

parseWhile = do
  symb "while"
  e <- parseExpr
  symb "{"
  stmts <- parseProg
  symb "}"
  return $ WhileStmt e stmts

parsePutStr = do
  symb "putStr("
  e <- parseExpr
  symb ");"
  return $ PutStr e
