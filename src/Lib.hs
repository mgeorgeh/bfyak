module Lib where

import MParsec
import BFYakParser
import Compiler
import BFPlayground

compile = Compiler.compile
getProg = BFYakParser.getProg

testCompile p = runPlayground ((\(_,x,_) -> x) $ runBF p emptyWorld) ""
