module Interpreter where

import MParsec
import System.Environment
import Data.List
import System.IO

type BFProg = [BFStatement] 

data BFStatement = Increment
                 | Decrement
                 | RightPtr
                 | LeftPtr
                 | GetChar
                 | PutChar
                 | Loop BFProg deriving (Show,Eq)

--printf '\33c\e[3J'
--last int is the farthest right the pointer has traveled
data BFWorld = World [Int] Int [Int] Int deriving (Show)

prettyPrint :: BFWorld -> String
prettyPrint (World as i bs l) =
  "["
  ++ concat (intersperse " " (map show (reverse as)))
  ++ " '"
  ++ show i
  ++ concatMap (\s -> " " ++ show s) (take (l - (length as) - 1) bs)
  ++ "]"
--  ++ "]"

maxLen (World _ _ _ i) = i

emptyWorld = World [] 0 (repeat 0) 0

main = runInterpret emptyWorld

runInterpret w = do
  putStr $ prettyPrint w ++ "> "
  hFlush stdout
  l <- getLine
  case l of
   "r" -> runInterpret emptyWorld
   "q" -> return ()
   bf -> do
     w' <- runCmdsAcc (parseBF.(filter (not.isWhiteSpace)) $ bf) w
     runInterpret w'

main1 :: IO ()
main1 = do
  args <- getArgs
  file <- readFile (args !! 0)
  execute.parseBF.(filter (not.isWhiteSpace)) $ file

isWhiteSpace c = elem c " \t\n"

execute :: BFProg -> IO ()
execute ss = runCmds ss emptyWorld

runCmds :: BFProg -> BFWorld -> IO ()
runCmds [] _ = return ()
runCmds (s:ss) w = do
  w' <- runCmd s w
  runCmds ss w'

runCmdsAcc [] w = return w
runCmdsAcc (s:ss) w = do
  w' <- runCmd s w
  runCmdsAcc ss w'

runCmd :: BFStatement -> BFWorld -> IO BFWorld
runCmd s w = case s of
  Increment -> return (increment w)
  Decrement -> return (decrement w)
  RightPtr -> return (rightPtr w)
  LeftPtr -> return (leftPtr w)
  GetChar -> do {c <- getChar; return (writeChar c w)}
  PutChar -> putChar (charAtPoint w) >> return w
  Loop stmts -> execLoop stmts w

execLoop :: BFProg -> BFWorld -> IO BFWorld
execLoop ss w = if loopCond w
                then return w
                else do {w' <- runCmdsAcc ss w; execLoop ss w'}
 
increment :: BFWorld -> BFWorld
increment (World as i bs l) = World as (i+1) bs l

decrement :: BFWorld -> BFWorld
decrement (World as i bs l) = World as (i-1) bs l

rightPtr :: BFWorld -> BFWorld

rightPtr (World as i (b:bs) l)
  | (length as + 1) > l = World (i:as) b bs (length as + 1)
  | otherwise = World (i:as) b bs l

{-rightPtr (World as i (b:bs) l)
  | (length as + 1) > l = World (as ++ [i]) b bs (length as + 1)
  | otherwise = World (as ++ [i]) b bs l-}

{-leftPtr :: BFWorld -> BFWorld
leftPtr (World as i bs l) = World (init as) (last as) (i:bs) l-}

leftPtr :: BFWorld -> BFWorld
leftPtr (World (a:as) i bs l) = World as a (i:bs) l

writeChar :: Char -> BFWorld -> BFWorld
writeChar c (World as _ bs l) = World as (fromEnum c) bs l

charAtPoint :: BFWorld -> Char
charAtPoint (World _ i _ _) = toEnum i

loopCond :: BFWorld -> Bool
loopCond (World _ 0 _ _) = True
loopCond _ = False


{-PARSER STUFF -}
parseBF :: String -> BFProg
parseBF s = fst $ head $ parse bfProg s

bfProg :: Parser BFProg
bfProg = many1 bfStatement

bfStatement :: Parser BFStatement
bfStatement = singleton <|> loop

singleton :: Parser BFStatement
singleton = fmap matchChar $ oneOf "+-><,."

matchChar c = case c of
  '+' -> Increment
  '-' -> Decrement
  '>' -> RightPtr
  '<' -> LeftPtr
  ',' -> GetChar
  '.' -> PutChar
  _  -> error $ "invalid character: " ++ (show c)

loop :: Parser BFStatement
loop = do
  char '['
  s <- bfProg
  char ']'
  return (Loop s)
