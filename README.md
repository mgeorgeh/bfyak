# README #

BFYak is a transpiler that converts a small C-like language into Brainfuck. The output is meant to be input to the [bff Brainfuck compiler](https://github.com/apankrat/bff)

The name comes from the fact that this project is a Yak that I had to shave in order to solve a different problem.
 
### Code Examples ###
BFYak supports the following features:

Variable declaration

    int i;
    uint j;
    char c;
    bool b;
    int[2][3] arr;

Variable assignment

    i = 17;
    j = u32;
    c = 'k';
    b = true;
    arr = { {1,2},{3,4},{5,6} }; 

Expression evaluation

    i = 2 * (20 - 5) / (3 + 3);
    b = true || false && !b;
    
Array indexing;

    i = 1;
    arr[i] = {6,6};

Loops
    
    while i < 20 {
      i = i + 1;
    } 

If statements

    if b {
      i = 2;
    } else {
      i = 3;
    }

Reading from stdin

    char[3] s;
    s = gets(3);

Writing to stdout

    putStr({'H','e','l','l','o'});