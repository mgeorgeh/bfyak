module Main where

import Lib (compile, getProg)
import System.Environment

main :: IO ()
main = writeFile "output.bf" =<< (compile . getProg) <$> (readFile =<< head <$> getArgs)
